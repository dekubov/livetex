<?php
/**
 * ���������� ����������
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class notes_Set extends set {
	
	/**
	 * ����� ����������
	 */
	public function text( $value ){
		
		if(!empty( $value )) $this->setValue( 'text', $value );
	}
	
	/**
	 * ��� �������������� ��������
	 */
	public function elemType( $value ){
	
		$element = array( 'contact' => 1, 'lead' => 2 );
		if(!empty( $value )) $this->setValue( 'element_type', $element[$value] );
	}
	
	/**
	 * ID �������������� ��������
	 */
	public function elemId( $value ){
	
		if(!empty( $value )) $this->setValue( 'element_id', $value );
	}
	
	/**
	 * ��� ����������
	 */
	public function type( $value ){
	
		if(!empty( $value )) $this->setValue( 'note_type', $value );
	}	
	
	/**
	 * �������������
	 */
	public function respId( $value ){

		if(!empty( $value )) $this->setValue( 'responsible_user_id', $value );
	}
}
?>