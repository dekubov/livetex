<?php
/**
 * Получение контактов
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class contacts_Get extends get {
	
	/**
	 * Получение всех контактов
	 */
	public function all(){
		
		$this->clear();
		$this->setParam( 'type', 'contact' );
		return $this->run();
	}
	
	/**
	 * Поиск контактов
	 */
	public function search( $query, $name = false ){
	
		$this->clear();
		$this->setParam( 'type', 'contact' );
		
		if( $name ) $query = $this->format( $query, $name );
		$this->setParam( 'query', $query );
		$result = $this->run();

		if( isset( $result[0] )){
		
			foreach( $result as $contact ){
				
				if( !isset( $contact->custom_fields )) continue;
				
				foreach( $contact->custom_fields as $cfield ){
					
					if( $name && $cfield->name != $name ) continue;
					
					foreach( $cfield->values as $cf_item ){
						
						if( $this->format( $cf_item->value, $name ) == $query ) return $contact;
					}
				}
			}
		} else return false;
	}
	
	/**
	 * Получение по ID
	 */
	public function byId( $id ){
	
		$this->clear();
		$this->setParam( 'type', 'contact' );
		$this->setParam( 'id', $id );
		$this->setParam( 'limit_rows', 1 );
		
		if( $data = $this->run()){
			if( is_array($data) && count($data) === 1 ) return $data[0];
			return $data;
		}
		return '';
	}
	
	/**
	 * Форматирование поискового запроса
	 */
	public function format( $query, $name = false ){
	
		if( $name == 'Телефон' && strlen( $query ) >= 10 ){
			$query = preg_replace('#[\s\-\(\)]+#Uis', '', $query );
			$query = strrev( substr( strrev( $query ), 0, 10 ));
		}
	  return $query;
	}
}
