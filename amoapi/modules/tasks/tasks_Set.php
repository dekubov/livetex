<?php
/**
 * ���������� �����
 *
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
class tasks_Set extends set {
	
	/**
	 * ����� ������
	 */
	public function text( $value ){
		
		if(!empty( $value )) $this->setValue( 'text', $value );
	}
	
	/**
	 * ��� �������������� ��������
	 */
	public function elemType( $value ){
	
		$element = array( 'contact' => 1, 'lead' => 2 );
		if(!empty( $value )) $this->setValue( 'element_type', $element[$value] );
	}
	
	/**
	 * ID �������������� ��������
	 */
	public function elemId( $value ){
	
		if(!empty( $value )) $this->setValue( 'element_id', $value );
	}
	
	/**
	 * ��� ������
	 */
	public function type( $value ){
	
		if( isset( $this->task_type[$value] )) $this->setValue( 'task_type', $this->task_type[$value] );
	}	
	
	/**
	 * ����� �� ������ � �������
	 */
	public function toTime( $value ){
	
		$time = time()+($value*60);
		if(!empty( $value )) $this->setValue( 'complete_till', $time );
	}
	
	/**
	 * ����� �� ������ timestamp
	 */
	public function setTime( $value ){
	
		if(!empty( $value )) $this->setValue( 'complete_till', $value );
	}
	
	/**
	 * ����� �� ������ Y-m-d H:i:s
	 */
	public function setDate( $value ){
	
		if(!empty( $value )) $this->setValue( 'complete_till', strtotime( $value ));
	}
	
	/**
	 * �������������
	 */
	public function respId( $value ){

		if(!empty( $value )) $this->setValue( 'responsible_user_id', $value );
	}
}
?>