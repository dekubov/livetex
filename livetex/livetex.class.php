<?php
/**
 * Класс для работы с LiveTex
 *
 * @name LiveTex
 * @author Evgeny Vakhrushev <ev@f5.com.ru>
 * @version 1.0
 */
class LiveTex {
    /**
	 * url запроса
     */
	private $baseurl="http://apiv2.livetex.ru/v2/";
    private $settings;
    private $token;
	
	//=======================================================
	
	public function __construct( $account = false ){
	
		define( 'APP', dirname(__FILE__));
		define( 'DULES', APP.'/modules');
		define( 'TEMP', APP.'/temp');
		/**
		 * Установка данных по аккаунту
		 */
		if( !$account ) include( APP.'/config/account.php');
        $this->settings=$account;
		/**
		 * Первичная инициализация
		 */
		$this->init();
	}
	
	/**
	 * Первичная инициализация
	 */
	private function init(){
        $url='https://apiv2.livetex.ru/v2/oauth2/token';
        $data="grant_type=password&username=".$this->settings['username']."&password=".$this->settings['password'];
        $result=$this->curl($url, $data);
        $this->token = $result->results->access_token;
	}
	
    public function chat_list($time='2016-02-25T08:00:00'){
        $url=$this->baseurl.'chats/list';
        $data="is_closed=true&q=created_at>".$time."&access_token=".$this->token;
        $result=$this->curl($url, $data);
        $chats_id=array();
        foreach ($result->results as $chat) {
            $chats_id[]=$chat->id;
        }
        return $chats_id;
    }
    
    public function chat_show($id){
        $url=$this->baseurl.'chats/show';
        $data="id=".$id."&access_token=".$this->token;
        return $this->curl($url, $data);
    }

    function curl($url, $data){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($curl, CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookieis.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookieis.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);
        $this->error($code, $url.$data);
        $response = json_decode($out);
        return $response;
    }
	
    /**
	 * Обработка кодов ошибок
     */
    private function error($code, $id ='')
    {
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            
            if($code!=200 && $code!=204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
        }
        catch(Exception $E)
        {
            print($id . ' LiveTex Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());

        }
    }
}
