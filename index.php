<?php
define( 'ROOT', dirname(__FILE__));
header('Access-Control-Allow-Origin: *'); 

include ROOT.'/livetex/livetex.class.php';
include ROOT.'/amoapi/amo.class.php';

$amo = new amoCRM();
$livetex = new LiveTex();

$file="time.txt";
$fp = fopen($file, "r");
$time=fread($fp, filesize($file));
fclose($fp);

$chats_id=$livetex->chat_list($time);

if(!empty($chats_id)){

	foreach ($chats_id as $n=>$id) {
		sleep(1);
		$chat=$livetex->chat_show($id);
		$last_created_at=$chat->results->created_at;
		$vid=$chat->results->visitor_id;
		$mas[$n]['name']=$chat->results->visitor->$vid->name;
		foreach ($chat->results->prechats_chat as $key => $value) {
			$mas[$n]['phone']=$value->value;
		}
		$mas[$n]['manager']=$chat->results->employee[0]->first_name.' '.$chat->results->employee[0]->last_name;

		foreach ($chat->results->messages as $message) {
			
			if($message->employee_id==1) $user="Посетитель : ";
			else $user=$mas[$n]['manager'].' : ';
			$mas[$n]['message'][]=$user.$message->text;
		}

		$time=strtotime($last_created_at);
		$time+=60;
		$time=date('Y-m-dTH:i:s', $time);

		$fp = fopen("time.txt", "w");
		fwrite($fp, $time);
		fclose($fp);

		// ищем контакт по номеру телефона
		if(!empty( $mas[$n]['phone'] )){
			$contact = $amo->contacts_Get->search( $mas[$n]['phone'], 'Телефон' );
		}

		// если контакт найден
		if( is_object($contact) && isset( $contact->id )){
			$contact_id = $contact->id;
			$resp_id = $contact->responsible_user_id;
			// если контакт не найден
		} else {
			$contact_id = false;
			$resp_id = 213444;
		}

		// добавление контакта
		if( !$contact_id ){
			
			// создание сделки
			$leadSet = $amo->leads_Set;
			$leadSet->name( 'Заявка с online консультанта ' );
			$leadSet->status( 'Новая' );
			$leadSet->respId( $resp_id );
			$leadSet->price( 0 );
			$lead_id = $leadSet->run();
			
			// создание контакта
			$setContact = $amo->contacts_Set;
			$setContact->name( $mas[$n]['name'] );
			$setContact->respId( $resp_id );
			$setContact->leads( $lead_id );
			$setContact->phone( $mas[$n]['phone'] );
			$contact_id = $setContact->run();
			
			// создание задачи
			$taskSet = $amo->tasks_Set;
			$taskSet->respId( $resp_id );
			$taskSet->text( 'Обработать контакт.' );
			$taskSet->type( 'Follow-up' );
			$taskSet->elemType( 'lead' );
			$taskSet->elemId( $lead_id );
			$taskSet->toTime( 60 );
			$taskSet->run();
			
			// текстовое примечание
			$noteSet = $amo->notes_Set;
			$noteSet->respId( $resp_id );
			$noteSet->text( "Чат: \r\n".implode("\r\n", $mas[$n]['message']) );
			$noteSet->type( 4 );
			$noteSet->elemType( 'lead' );
			$noteSet->elemId( $lead_id );
			$noteSet->run();

			// работа с найденным контактом
		} else {
			// создание сделки
			$leadSet = $amo->leads_Set;
			$leadSet->name( 'Заявка с online консультанта ' );
			$leadSet->status( 'Новая' );
			$leadSet->respId( $resp_id );
			$leadSet->price( 0 );
			$lead_id = $leadSet->run();
			
			// обновление контакта
			$UpdContact = $amo->contacts_Update;
			$UpdContact->setContact( $contact );
			$UpdContact->leads( $lead_id );
			$contact_id = $UpdContact->run();
			
			// создание задачи
			$taskSet = $amo->tasks_Set;
			$taskSet->respId( $resp_id );
			$taskSet->text( 'Обработать контакт.' );
			$taskSet->type( 'Follow-up' );
			$taskSet->elemType( 'lead' );
			$taskSet->elemId( $lead_id );
			$taskSet->toTime( 60 );
			$taskSet->run();
			
			// текстовое примечание
			$noteSet = $amo->notes_Set;
			$noteSet->respId( $resp_id );
			$noteSet->text( "Чат: \r\n".implode("\r\n", $mas[$n]['message']) );
			$noteSet->type( 4 );
			$noteSet->elemType( 'lead' );
			$noteSet->elemId( $lead_id );
			$noteSet->run();
		}

	}
}